import datetime

from flask import Request, Response, jsonify

from mongo_connection import collection


class ImageRequestValidator:
    possible_statuses = ['new', 'review', 'accepted', 'deleted']

    def validate_put_request(self, request: Request) -> dict or None:
        url = request.json.get('url')
        if url is None:
            return {'Error': f'url field can not be empty.'}

        status = request.json.get('status')
        if status is None:
            return {'Error': 'status field can not be empty.'}
        elif status not in self.possible_statuses:
            return {'Error': f'status value can be one of the following: {self.possible_statuses}'}

    def validate_get_request(self, request: Request) -> dict or None:
        possible_args = ['status']
        for arg in request.args.keys():
            if arg not in possible_args:
                return {'Error': f'{arg} - is not recognizable. Possible args: {possible_args}'}

    def run(self, request: Request) -> dict or None:
        if request.method == 'PUT':
            error = self.validate_put_request(request)
        else:
            error = self.validate_get_request(request)

        return error


class ImageProcessor:
    def get(self, request: Request) -> Response:
        pipeline = [{'$unwind': {'path': '$images'}}]

        if len(request.args.keys()) > 0:
            pipeline.append(
                {'$match': {
                    'images.status': {
                        '$eq': request.args.get('status')
                    }
                }}
            )

        pipeline.append({'$sort': {'images.created_at': -1}})
        pipeline.append({'$group': {
                '_id': '$name',
                'count': {'$sum': 1},
                'images': {'$push': '$images'}
            }})

        mongo_data = collection.aggregate(pipeline)
        response = jsonify(list(mongo_data))

        return response

    def put(self, request: Request) -> Response:
        collection.update_many(
            {'images.url': request.json.get('url')},
            {'$set': {'images.$.status': request.json.get('status')}}
        )

        return jsonify({'status': 'ok'})

    def run(self, request: Request) -> Response:
        if request.method == 'PUT':
            response = self.put(request)
        else:
            response = self.get(request)

        return response


class ImageStatsProcessor:
    def run(self, request: Request) -> Response:
        stats_period_start = datetime.datetime.now()-datetime.timedelta(days=30)
        pipeline = [
            {'$unwind': {'path': '$images'}},
            {'$match': {
                'images.created_at': {
                    '$gt': stats_period_start
                }
            }},
            {'$group': {
                '_id': '$images.status',
                'count': {'$sum': 1}
            }},
        ]

        mongo_data = collection.aggregate(pipeline)

        # Well I have done all the time wasting work in the mongo query
        # So for better response view I do some python work
        response = dict()
        for status in list(mongo_data):
            response[status.get('_id')] = status.get('count')

        return jsonify(response)
