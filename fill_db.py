from datetime import datetime

from mongo_connection import collection

test_data = [
    {'name': 'first_group', 'images': [
        {'created_at': datetime.strptime('2023-05-08T10:53:53.000Z', '%Y-%m-%dT%H:%M:%S.000Z'), 'url': 'url1',
         'status': 'new'},
        {'created_at': datetime.strptime('2023-05-09T10:53:53.000Z', '%Y-%m-%dT%H:%M:%S.000Z'), 'url': 'url2',
         'status': 'review'},
        {'created_at': datetime.strptime('2023-05-07T10:53:53.000Z', '%Y-%m-%dT%H:%M:%S.000Z'), 'url': 'url3',
         'status': 'accepted'},
    ]},
    {'name': 'second_group', 'images': [
        {'created_at': datetime.strptime('2023-04-04T10:53:54.000Z', '%Y-%m-%dT%H:%M:%S.000Z'), 'url': 'url4',
         'status': 'new'},
        {'created_at': datetime.strptime('2023-04-09T10:53:53.000Z', '%Y-%m-%dT%H:%M:%S.000Z'), 'url': 'url5',
         'status': 'deleted'},
        {'created_at': datetime.strptime('2017-10-19T10:53:53.000Z', '%Y-%m-%dT%H:%M:%S.000Z'), 'url': 'url6',
         'status': 'deleted'},
    ]},
]

for doc in test_data:
    collection.insert_one(doc)
