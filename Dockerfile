FROM python:3.8

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install --upgrade pip
RUN pip3 install -r  \
    requirements.txt \
    --no-cache

COPY . .

CMD ["python3", "server.py"]
