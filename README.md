## Prerequisites
Python 3.8+ installed
Mongo db launched locally on default port (if you want to test fully)
```bash
python3 fill_db.py  ## If you want to test fully and fill your DB
```

## Installation
```bash
pip3 install -r requirements.txt
```

## Launch
```bash
python3 server.py
pytest ## Launch tests. Tests need server running
```


## Some notes
1. Dockerfile just exists. For production usage better to use gunicorn for launch
2. Tests will work only if webserver is running. 
This might be useful if we launch our webserver in test environment and launch tests on a separate machine
This can be made in another way, using flask test client if needed
3. For validation might be used external validation packages, I just tried to make it simple