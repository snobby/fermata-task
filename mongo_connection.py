from pymongo import MongoClient

client = MongoClient()  # Local database is used on default port
db = client['fermata-db']
collection = db['fermata-collection']