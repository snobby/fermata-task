import os

from flask import Flask, request

from processing import ImageProcessor, ImageStatsProcessor, ImageRequestValidator

app = Flask(__name__)

app.config.update(
    DEBUG=bool(os.environ.get('DEBUG', True)),
)

with app.app_context():
    image_processor = ImageProcessor()
    stats_processor = ImageStatsProcessor()
    image_request_validator = ImageRequestValidator()


@app.route('/image', methods=['GET', 'PUT'])
def image():
    validation_error = image_request_validator.run(request)
    if validation_error is not None:
        response = validation_error, 400
    else:
        response = image_processor.run(request), 200

    return response


@app.route('/image_stats', methods=['GET'])
def image_stats():
    response = stats_processor.run(request), 200

    return response


with app.app_context():
    if app.config['DEBUG'] is True:
        app.run(debug=True)
