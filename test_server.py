import os

import requests


class TestApp:
    test_uri = os.environ.get('TEST_SERVER_URI', 'http://0.0.0.0:5000')
    stats_endpoint = '/image_stats'
    image_endpoint = '/image'
    timeout = 10

    def test_image_get_endpoint(self):
        url = f'{self.test_uri}{self.image_endpoint}'
        response = requests.get(url=url)
        assert response.status_code == 200

        response = requests.get(url=url, params={'status': 'new'})
        assert response.status_code == 200

        response = requests.get(url=url, params={'status': 'some_new_status'})
        # If we try to filter by non existing status value we will receive empty results but the answer will be 200
        assert response.status_code == 200

        response = requests.get(url=url, params={'wrong_param': 'new'})
        assert response.status_code == 400

        response = requests.post(url=url, json={'some_data': 'some_value'})
        assert response.status_code == 405

    def test_image_put_endpoint(self):
        url = f'{self.test_uri}{self.image_endpoint}'
        response = requests.put(url=url)
        assert response.status_code == 415

        response = requests.put(url=url, json={'wrong_param_name': 'some_value'})
        assert response.status_code == 400
        assert response.json() == {'Error': 'url field can not be empty.'}

        response = requests.put(url=url, json={'url': 'test_url_value'})
        assert response.status_code == 400
        assert response.json() == {'Error': 'status field can not be empty.'}

        response = requests.put(url=url, json={'url': 'test_url_value', 'status': 'wrong_status_value'})
        assert response.status_code == 400
        assert response.json() == {
            'Error': "status value can be one of the following: ['new', 'review', 'accepted', 'deleted']"
        }

        response = requests.put(url=url, json={'url': 'test_url_value', 'status': 'new'})
        assert response.status_code == 200
        assert response.json() == {'status': 'ok'}
        # This is dangerous test because it can affect production database
        # We can test document in tests, or catch this request in webserver (with some test header for example)

    def test_stats_image_endpoint(self):
        url = f'{self.test_uri}{self.stats_endpoint}'
        response = requests.get(url=url)
        assert response.status_code == 200

        response = requests.post(url=url, json={'some_data': 'some_value'})
        assert response.status_code == 405